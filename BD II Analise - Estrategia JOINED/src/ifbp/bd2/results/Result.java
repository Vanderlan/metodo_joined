
package ifbp.bd2.results;

/**
 *
 * @author vanderlan
 */
public class Result {
    
    
    private double time;
    private int inserts;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public int getInserts() {
        return inserts;
    }

    public void setInserts(int inserts) {
        this.inserts = inserts;
    }
    
    
}
