

package ifbp.bd2.results;

import ifbp.bd2.util.HibernateUtil;
import ifpb.bd2.entities.Aluno;
import ifpb.bd2.entities.GenericDAO;
import java.util.Date;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author vanderlan
 */
public class Test {
    
    
    public Result testarInserts(int inserts){
        
        Result result = new Result();
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction transaction = session.getTransaction();
        
        Date dataNas = new Date(System.currentTimeMillis());
        
        double inicio = System.currentTimeMillis();
        for(int i = 0; i < inserts; i++){
            
            Aluno aluno = new Aluno();
            aluno.setNome("Vanderlan Gomes da Silva");
            aluno.setCpf("10145473422");
            aluno.setPerido(4);
            aluno.setDataNascimento(dataNas);
            aluno.setMatricula("498329482308");
            aluno.setCurso("Análise e Desenvolvimento de Sistemas");
            aluno.setEmail("vanderlan.14@hotmail.com");
            aluno.setEndereco("Rua Antão Alves, 118, centro, Sertânia-PE");
            aluno.setRg("8862900");
            aluno.setSexo("M");
            aluno.setTelefone("38412217");
        
            transaction.begin();
            
            session.save(aluno);
            
            transaction.commit();
            
        }
        double fim = System.currentTimeMillis();
        double  time = fim - inicio;
        
        result.setInserts(inserts);
        
        result.setTime(time/100);
        
        return result;
    }
     public Result testShreach(){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        Result result = new Result();
        
        Transaction transaction = session.getTransaction();
        
        GenericDAO dao = new GenericDAO(Aluno.class, session);
        
        double inicio = System.currentTimeMillis();
        transaction.begin();
            dao.getEntities();
        transaction.commit();
        double fim = System.currentTimeMillis();
        double  time = fim - inicio;
        
        result.setTime(time / 100);
        result.setInserts(dao.getEntities().size());
       
        return result;
    }
}