package ifpb.bd2.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Vanderlan
 */
@Entity
@Table(name="tecnico")
public class Tecnico extends Pessoa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "classe")
    private char classe;
    @Basic(optional = false)
    @Column(name = "formacao")
    private String formacao;
    @Basic(optional = false)
    @Column(name = "funcao")
    private String funcao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "salario")
    private BigDecimal salario;

    public Tecnico() {
    }


    public Tecnico(char classe, String formacao, String funcao, BigDecimal salario) {
        this.classe = classe;
        this.formacao = formacao;
        this.funcao = funcao;
        this.salario = salario;
    }

    public char getClasse() {
        return classe;
    }

    public void setClasse(char classe) {
        this.classe = classe;
    }

    public String getFormacao() {
        return formacao;
    }

    public void setFormacao(String formacao) {
        this.formacao = formacao;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

}
