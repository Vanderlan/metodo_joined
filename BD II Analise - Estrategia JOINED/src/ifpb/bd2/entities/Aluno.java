
package ifpb.bd2.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Vanderlan
 */
@Entity
@Table(name = "aluno")
public class Aluno extends Pessoa implements Serializable{
    
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "periodo")
    private int perido;
    @Basic(optional = false)
    @Column(name = "curso")
    private String curso;

    public Aluno() {
    }


    public Aluno( int perido, String curso) {
        this.perido = perido;
        this.curso = curso;
    }

    public int getPerido() {
        return perido;
    }

    public void setPerido(int perido) {
        this.perido = perido;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

}
