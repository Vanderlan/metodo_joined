package ifpb.bd2.entities;

import java.io.Serializable;
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;

/**
 *
 * @author Vanderlan
 */
public interface InterfadeDAO<T> {

    public void save(T entity);
    public void update(T entity);
    public void remove(T entity);
    public void merge(T entity);
    public T getEntity(Serializable id);
    public T getEntityDetachedCriteria(DetachedCriteria criteiria);
    List<T> getEntities();
    List<T> getListByDetachedCriteria(DetachedCriteria criteiria);
}
