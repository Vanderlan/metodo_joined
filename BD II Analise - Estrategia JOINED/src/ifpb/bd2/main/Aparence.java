/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ifpb.bd2.main;

import ifbp.bd2.results.FrameResult;

/**
 *
 * @author vanderlan
 */
public class Aparence {
     public static void lookAndfeel(){
        try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                                javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                break;

                        }
                }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(FrameResult.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
}
}
